INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (4,'mare@gmail.com','mare','$2a$10$hW5o6Trv06hWFfqfm2zN9.foYgCIcxwVl7wGWUo0QMdiCLOsIpnk2','Marko','Markovic','ADMIN');
              
INSERT INTO format (id, broj_ucesnika, tip) VALUES (1, 3, 'Grand slam');
INSERT INTO format (id, broj_ucesnika, tip) VALUES (2, 12, 'Masters 1000');
INSERT INTO format (id, broj_ucesnika, tip) VALUES (3, 10, 'Masters 500');
INSERT INTO format (id, broj_ucesnika, tip) VALUES (4, 10, 'Masters 250');

INSERT INTO takmicenje (id, datum_pocetka, datum_zavrsetka, mesto, naziv, format_id) VALUES (1, '2022-05-05', '2022-05-25', 'Paris,FRA',
'Roland-Garros', 1);
INSERT INTO takmicenje (id, datum_pocetka, datum_zavrsetka, mesto, naziv, format_id) VALUES (2, '2022-06-15', '2022-06-25', 'Rome,ITA',
'Rome 1000', 2);
INSERT INTO takmicenje (id, datum_pocetka, datum_zavrsetka, mesto, naziv, format_id) VALUES (3, '2022-07-05', '2022-07-25', 'London,GBR',
'Wimbledon', 1);
INSERT INTO takmicenje (id, datum_pocetka, datum_zavrsetka, mesto, naziv, format_id) VALUES (4, '2022-05-15', '2022-05-25', 'Roterdam,NED',
'Roterdam 500', 3);
 



