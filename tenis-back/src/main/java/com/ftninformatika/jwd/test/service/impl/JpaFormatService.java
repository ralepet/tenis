package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Format;
import com.ftninformatika.jwd.test.repository.FormatRepository;
import com.ftninformatika.jwd.test.service.FormatService;

@Service
public class JpaFormatService implements FormatService{
	
	@Autowired
	private FormatRepository formatRepository;

	@Override
	public Format findOneById(Long id) {
		return formatRepository.findOneById(id);
	}

	@Override
	public List<Format> findAll() {
		return formatRepository.findAll();
	}


}
