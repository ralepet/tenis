package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.PrijavaDto;
import com.ftninformatika.jwd.test.model.Prijava;

@Component
public class PrijavaToPrijavaDto implements Converter<Prijava, PrijavaDto>{

	@Override
	public PrijavaDto convert(Prijava prijava) {
		PrijavaDto dto = new PrijavaDto();
		dto.setId(prijava.getId());
		dto.setDatumPrijave(prijava.getDatumPrijave().toString());
		dto.setDrzava(prijava.getDrzava());
		//dto.setKorisnikId(prijava.getKorisnik().getId());
		dto.setTakmicenjeId(prijava.getTakmicenje().getId());
		dto.setTakmicenjeNaziv(prijava.getTakmicenje().getNaziv());
		dto.setKontakt(prijava.getKontakt());
		
		return dto;
	}
	
	public List<PrijavaDto> convert(List<Prijava>list){
		List<PrijavaDto>dto = new ArrayList<>();
		for(Prijava prijava : list) {    
			dto.add(convert(prijava));
		}
		return dto;
	}

}
