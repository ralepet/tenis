package com.ftninformatika.jwd.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.TakmicenjeDto;
import com.ftninformatika.jwd.test.model.Takmicenje;
import com.ftninformatika.jwd.test.service.TakmicenjeService;
import com.ftninformatika.jwd.test.support.TakmicenjeDtoToTakmicenje;
import com.ftninformatika.jwd.test.support.TakmicenjeToTakmicenjeDto;

@RestController
@RequestMapping(value = "/api/takmicenja", produces = MediaType.APPLICATION_JSON_VALUE)
public class TakmicenjeController {
	
	@Autowired
	private TakmicenjeService takmicenjeService; 

	@Autowired
	private TakmicenjeToTakmicenjeDto toDto;
	
	@Autowired
	private TakmicenjeDtoToTakmicenje toTakmicenje;
	
	//@PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<TakmicenjeDto>> getAll(
			@RequestParam(required=false) String mesto, 
			@RequestParam(required=false) Long formatId,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		
		// putanja http://localhost:8080/api/linije?pageNo=0 ili neki drugi broj umesto 0
		
		//Page<Linija> page = linijaService.findAll(pageNo);

		Page<Takmicenje> page = takmicenjeService.find(mesto, formatId, pageNo);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
	
	// @PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping("/{id}")
	public ResponseEntity<TakmicenjeDto> getOne(@PathVariable Long id) {
		Takmicenje takmicenje = takmicenjeService.findOneById(id);

		if (takmicenje != null) {
			return new ResponseEntity<>(toDto.convert(takmicenje), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	// @PreAuthorize("hasAnyRole('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TakmicenjeDto> create(@Valid @RequestBody TakmicenjeDto takmicenjeDto) { 
		Takmicenje takmicenje = toTakmicenje.convert(takmicenjeDto); 

		if (takmicenje.getFormat() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} 

		Takmicenje sacuvanoTakmicenje = takmicenjeService.save(takmicenje);

		return new ResponseEntity<>(toDto.convert(sacuvanoTakmicenje), HttpStatus.CREATED);
	}
	
	// @PreAuthorize("hasAnyRole('ADMIN')")
	@PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TakmicenjeDto> update(@PathVariable Long id, @Valid @RequestBody TakmicenjeDto takmicenjeDto) {

		if (!id.equals(takmicenjeDto.getId())) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Takmicenje takmicar = toTakmicenje.convert(takmicenjeDto); 
		Takmicenje sacuvanoTakmicenj = takmicenjeService.update(takmicar);

		return new ResponseEntity<>(toDto.convert(sacuvanoTakmicenj), HttpStatus.OK);
	}
	

	// @PreAuthorize("hasAnyRole('ADMIN')")
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		Takmicenje obrisanoTakmicenje = takmicenjeService.delete(id); 

		if (obrisanoTakmicenje != null) {
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	@ExceptionHandler(value = DataIntegrityViolationException.class)
	public ResponseEntity<Void> handle() {
		return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
	}
	
	
	

}
