package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.FormatDto;
import com.ftninformatika.jwd.test.model.Format;



@Component
public class FormatToFormatDto implements  Converter<Format, FormatDto>{
	
	@Override
	public FormatDto convert(Format format) { 
		FormatDto dto = new FormatDto();
		dto.setId(format.getId());
		dto.setBrojUcesnika(format.getBrojUcesnika());
		dto.setTip(format.getTip());
		return dto;
	}
	
	public List<FormatDto> convert(List<Format>list){
		List<FormatDto>dto = new ArrayList<>();
		for(Format format : list) {  
			dto.add(convert(format));
		}
		return dto;
	}

}
