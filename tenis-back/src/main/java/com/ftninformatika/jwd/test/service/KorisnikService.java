package com.ftninformatika.jwd.test.service;

import com.ftninformatika.jwd.test.dto.KorisnikPromenaLozinkeDto;
import com.ftninformatika.jwd.test.model.Korisnik;

import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

public interface KorisnikService {
	
	Korisnik findOneById(Long id);

    Optional<Korisnik> findOne(Long id);

    List<Korisnik> findAll();

    Page<Korisnik> findAll(int brojStranice);

    Korisnik save(Korisnik korisnik);

    void delete(Long id);

    Optional<Korisnik> findbyKorisnickoIme(String korisnickoIme);

    boolean changePassword(Long id, KorisnikPromenaLozinkeDto korisnikPromenaLozinkeDto);
}
