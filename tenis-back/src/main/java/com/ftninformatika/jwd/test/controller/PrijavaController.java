package com.ftninformatika.jwd.test.controller;

import java.time.LocalDate;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.PrijavaDto;
import com.ftninformatika.jwd.test.model.Prijava;
import com.ftninformatika.jwd.test.service.PrijavaService;
import com.ftninformatika.jwd.test.support.PrijavaDtoToPrijava;
import com.ftninformatika.jwd.test.support.PrijavaToPrijavaDto;

@RestController
@RequestMapping(value = "/api/prijave", produces = MediaType.APPLICATION_JSON_VALUE)
public class PrijavaController {
	
	@Autowired
	private PrijavaService prijavaService;
	
	@Autowired
	private PrijavaToPrijavaDto toDto;
	
	@Autowired
	private PrijavaDtoToPrijava toPrijava;
	
	// @PreAuthorize("hasAnyRole('KORISNIK', 'ADMIN')")
	@GetMapping
	public ResponseEntity<List<PrijavaDto>> getAll() {

		List<Prijava> list = prijavaService.findAll();

		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PrijavaDto> create(@Valid @RequestBody PrijavaDto prijavaDto) { 
		Prijava prijava = toPrijava.convert(prijavaDto);
		prijava.setDatumPrijave(LocalDate.now());

		if (prijava.getTakmicenje() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		Prijava sacuvanaPrijava = prijavaService.save(prijava);
		
		if(sacuvanaPrijava == null){
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(toDto.convert(sacuvanaPrijava), HttpStatus.CREATED);
	}
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
	

}
