package com.ftninformatika.jwd.test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.test.model.Takmicenje;


public interface TakmicenjeService {
	
	Takmicenje findOneById(Long id);

	Takmicenje save(Takmicenje takmicenje);

	Takmicenje update(Takmicenje takmicenje);

	Takmicenje delete(Long id);

	Page<Takmicenje> findAll(Integer pageNo);

	List<Takmicenje> findAll();
	
	Page<Takmicenje> find(String mesto,  Long formatId, Integer pageNo);
	
	Integer dobaviBrojSlobodnihMesta(Takmicenje takmicenje);

}
