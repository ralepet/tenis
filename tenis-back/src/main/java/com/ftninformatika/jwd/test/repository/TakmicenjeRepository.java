package com.ftninformatika.jwd.test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.test.model.Takmicenje;

@Repository
public interface TakmicenjeRepository extends JpaRepository<Takmicenje, Long>{
	
	Takmicenje findOneById(Long id);
	
	Page<Takmicenje> findByMestoIgnoreCaseContains( String mesto,  Pageable pageable);
	
	Page<Takmicenje> findByMestoIgnoreCaseContainsAndFormatId( String mesto, Long formatId, Pageable pageable);

}
