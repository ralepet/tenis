package com.ftninformatika.jwd.test.support;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.TakmicenjeDto;
import com.ftninformatika.jwd.test.model.Format;
import com.ftninformatika.jwd.test.model.Takmicenje;
import com.ftninformatika.jwd.test.service.FormatService;
import com.ftninformatika.jwd.test.service.TakmicenjeService;

@Component
public class TakmicenjeDtoToTakmicenje implements Converter<TakmicenjeDto, Takmicenje>{
	
	@Autowired
	private TakmicenjeService takmicenjeService;
	
	@Autowired
	private FormatService formatService;
	
	@Override
	public Takmicenje convert(TakmicenjeDto dto) {
		Takmicenje takmicenje = null;

		if (dto.getId() != null) {
			takmicenje = takmicenjeService.findOneById(dto.getId());
		}
		
		if(takmicenje == null) {
			takmicenje = new Takmicenje();
        }
		 
		Format format = formatService.findOneById(dto.getFormatId());
		if(format != null) {
			takmicenje.setFormat(format);
		}
		takmicenje.setDatumPocetka(getLocalDate(dto.getDatumPocetka()));
		takmicenje.setDatumZavrsetka(getLocalDate(dto.getDatumZavrsetka()));
		takmicenje.setNaziv(dto.getNaziv());
		takmicenje.setMesto(dto.getMesto());		
		
		return takmicenje;
	}
	
	private LocalDate getLocalDate(String date) throws DateTimeParseException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(date, formatter);
    }

}
