package com.ftninformatika.jwd.test.dto;

import javax.validation.constraints.Size;

public class TakmicenjeDto {
	
	private Long id;
	
	@Size(max = 50)
	private String mesto; 
	
	private String naziv;
	
	private String datumPocetka;
	
	private String datumZavrsetka;
	
	private Long formatId;
	
	private String formatTip;
	
	private Integer brojSlobodnihMesta;
	

	public TakmicenjeDto() {
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getMesto() {
		return mesto;
	}


	public void setMesto(String mesto) {
		this.mesto = mesto;
	}


	public String getNaziv() {
		return naziv;
	}


	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}


	public String getDatumPocetka() {
		return datumPocetka;
	}


	public void setDatumPocetka(String datumPocetka) {
		this.datumPocetka = datumPocetka;
	}


	public String getDatumZavrsetka() {
		return datumZavrsetka;
	}


	public void setDatumZavrsetka(String datumZavrsetka) {
		this.datumZavrsetka = datumZavrsetka;
	}


	public Long getFormatId() {
		return formatId;
	}


	public void setFormatId(Long formatId) {
		this.formatId = formatId;
	}


	public String getFormatTip() {
		return formatTip;
	}


	public void setFormatTip(String formatTip) {
		this.formatTip = formatTip;
	}


	public Integer getBrojSlobodnihMesta() {
		return brojSlobodnihMesta;
	}


	public void setBrojSlobodnihMesta(Integer brojSlobodnihMesta) {
		this.brojSlobodnihMesta = brojSlobodnihMesta;
	}
	

}
