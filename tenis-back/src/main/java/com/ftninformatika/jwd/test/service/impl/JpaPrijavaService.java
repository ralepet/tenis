package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Prijava;
import com.ftninformatika.jwd.test.model.Takmicenje;
import com.ftninformatika.jwd.test.repository.PrijavaRepository;
import com.ftninformatika.jwd.test.service.PrijavaService;
import com.ftninformatika.jwd.test.service.TakmicenjeService;

@Service
public class JpaPrijavaService implements PrijavaService{

	@Autowired
	private PrijavaRepository prijavaRepository;
	
	@Autowired
	private TakmicenjeService takmicenjeService;

	@Override
	public Prijava findOneById(Long id) {
		return prijavaRepository.findOneById(id);
	}
	@Override
	public List<Prijava> findAll() {
		return prijavaRepository.findAll();
	}

	@Override
	public Prijava save(Prijava prijava) {
		
		
		Takmicenje takmicenje = prijava.getTakmicenje(); 
		int brojSlobodnihMesta = takmicenjeService.dobaviBrojSlobodnihMesta(takmicenje);
		if(brojSlobodnihMesta <= 0) {
			return null;
		}
		
		/* AKO SE ZELI DA ONEMOGUCU DA SE ISTI KORISNIK PRIJAVI NA VISE TAKMICENJA< OTKOMENTARISATI KOD
		 * 
		List<Prijava> svePrijave = prijavaRepository.findAll();
		for (Prijava itPrijava : svePrijave) {
			if (itPrijava.getKorisnik().getId() == prijava.getKorisnik().getId()) {
				return null;
			}
		}
		*/
		
		return prijavaRepository.save(prijava);
	}

}
