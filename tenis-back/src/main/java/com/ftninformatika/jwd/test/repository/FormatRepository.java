package com.ftninformatika.jwd.test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.test.model.Format;

@Repository
public interface FormatRepository extends JpaRepository<Format, Long>{
	
	Format findOneById(Long id);

}
