package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.Prijava;

public interface PrijavaService {
	
	Prijava findOneById(Long id);

    List<Prijava> findAll();

    Prijava save(Prijava prijava);

}
