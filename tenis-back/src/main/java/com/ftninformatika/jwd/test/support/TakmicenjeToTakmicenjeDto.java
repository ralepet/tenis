package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.TakmicenjeDto;
import com.ftninformatika.jwd.test.model.Takmicenje;
import com.ftninformatika.jwd.test.service.TakmicenjeService;

@Component
public class TakmicenjeToTakmicenjeDto implements Converter<Takmicenje, TakmicenjeDto>{
	
	@Autowired
	private TakmicenjeService takmicenjeService;

	@Override
	public TakmicenjeDto convert(Takmicenje source) {
		TakmicenjeDto dto = new TakmicenjeDto();
		dto.setId(source.getId());
		dto.setDatumPocetka(source.getDatumPocetka().toString());
		dto.setDatumZavrsetka(source.getDatumZavrsetka().toString());
		dto.setMesto(source.getMesto());
		dto.setFormatId(source.getFormat().getId());
		dto.setFormatTip(source.getFormat().getTip());
		dto.setNaziv(source.getNaziv());
		dto.setBrojSlobodnihMesta(takmicenjeService.dobaviBrojSlobodnihMesta(source));
		return dto;
	}
	
	public List<TakmicenjeDto> convert(List<Takmicenje>list){
		List<TakmicenjeDto>dto = new ArrayList<>();
		for(Takmicenje takmicenje : list) {   
			dto.add(convert(takmicenje)); 
		}
		return dto;
	}

}
