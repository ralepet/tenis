package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.Format;


public interface FormatService {
	Format findOneById(Long id);

	List<Format> findAll();

}
