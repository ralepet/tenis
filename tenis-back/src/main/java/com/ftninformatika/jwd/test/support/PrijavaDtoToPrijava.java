package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.PrijavaDto;
import com.ftninformatika.jwd.test.model.Prijava;
import com.ftninformatika.jwd.test.model.Takmicenje;
import com.ftninformatika.jwd.test.service.PrijavaService;
import com.ftninformatika.jwd.test.service.TakmicenjeService;

@Component
public class PrijavaDtoToPrijava implements Converter<PrijavaDto, Prijava>{
	
	@Autowired
	private PrijavaService prijavaService;
	
	@Autowired
	private TakmicenjeService takmicenjeService;
	
	/*
	@Autowired
	private KorisnikService korisnikService;
	*/

	@Override
	public Prijava convert(PrijavaDto dto) {
		Prijava prijava = null;
		
		if (dto.getId() != null) {
			prijava = prijavaService.findOneById(dto.getId());
		}
		
		if(prijava == null) {
			prijava = new Prijava();
        }
		
		prijava.setDrzava(dto.getDrzava());
		prijava.setKontakt(dto.getKontakt());
		
		/*
		Korisnik korisnik = korisnikService.findOneById(dto.getKorisnikId());
		if(korisnik != null) {
			prijava.setKorisnik(korisnik);
		}
		*/
		
		Takmicenje takmicenje = takmicenjeService.findOneById(dto.getTakmicenjeId());
		if(takmicenje != null) {
			prijava.setTakmicenje(takmicenje);
		}
		
		return prijava;
	}
	

}
