package com.ftninformatika.jwd.test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Takmicenje;
import com.ftninformatika.jwd.test.repository.TakmicenjeRepository;
import com.ftninformatika.jwd.test.service.TakmicenjeService;

@Service
public class JpaTakmicenjeService implements TakmicenjeService{
	
	@Autowired
	private TakmicenjeRepository takmicenjeRepository;

	@Override
	public Takmicenje findOneById(Long id) {
		return takmicenjeRepository.findOneById(id);
	}

	@Override
	public Takmicenje save(Takmicenje takmicenje) {
		return takmicenjeRepository.save(takmicenje);
	}

	@Override
	public Takmicenje update(Takmicenje takmicenje) {
		return takmicenjeRepository.save(takmicenje);
	}

	@Override 
	public Takmicenje delete(Long id) {
		Optional<Takmicenje> takmicenje = takmicenjeRepository.findById(id);
		if (takmicenje.isPresent()) {
			takmicenjeRepository.deleteById(id);
			return takmicenje.get();
		}
		return null;
	}

	@Override
	public Page<Takmicenje> findAll(Integer pageNo) {
		return takmicenjeRepository.findAll(PageRequest.of(pageNo, 3));
	}

	@Override
	public List<Takmicenje> findAll() {
		return takmicenjeRepository.findAll();
	}

	@Override
	public Page<Takmicenje> find(String mesto, Long formatId, Integer pageNo) {
		if(mesto == null) {
			mesto = "";
		}//
		
		if(formatId == null) {
			return takmicenjeRepository.findByMestoIgnoreCaseContains(mesto, PageRequest.of(pageNo, 3));
		}
		
		return takmicenjeRepository.findByMestoIgnoreCaseContainsAndFormatId(mesto, formatId, PageRequest.of(pageNo, 3));
	}

	@Override
	public Integer dobaviBrojSlobodnihMesta(Takmicenje takmicenje) {
		//Takmicenje takmicenje = takmicenjeRepository.findOneById(id);
		int ukupanBrojUcesnika= takmicenje.getFormat().getBrojUcesnika();

		int brojPrijava = 0;
		if (takmicenje.getPrijave() != null ) { // lista ce biti null kada se takmicenje kreira
			brojPrijava = takmicenje.getPrijave().size();
		} 

		return ukupanBrojUcesnika - brojPrijava;
	}

}
