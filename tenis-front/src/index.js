import React from 'react';
import ReactDOM from 'react-dom';
import { Navigate, Route, Link, HashRouter as Router, Routes } from 'react-router-dom';
import { Navbar, Nav, Container, Button } from 'react-bootstrap';
import Home from './components/Home';
import Login from './components/authorization/Login'

import Takmicenja from './components/takmicenja/Takmicenja';
import KreirajTakmicenje  from './components/takmicenja/KreirajTakmicenje';
import Prijava from './components/takmicenja/Prijava'; 

import NotFound from './components/NotFound';
import { logout } from './services/auth';


class App extends React.Component {

    render() {
        const jwt = window.localStorage.getItem("jwt")
        if(jwt){
            return (
                <div>
                    <Router>
                        <Navbar expand bg="dark" variant="dark">
                            <Navbar.Brand as={Link} to="/">
                                JWD
                            </Navbar.Brand>
                            <Nav>
                                <Nav.Link as={Link} to="/takmicenja">
                                    Takmicenja
                                </Nav.Link>
                            </Nav>
                            <Button onClick={() => logout()}>Logout</Button>
                        </Navbar>
                        <Container style={{ paddingTop: "10px" }}>
                            <Routes>
                                <Route path="/" element={<Home />} />
                                <Route path="/takmicenja" element={<Takmicenja />} />
                                {window.localStorage.getItem("role") === "ROLE_KORISNIK" ? <Route path="/takmicenja/prijava/:id" element={<Prijava />} /> 
                                : <Route path="/takmicenja/prijava/:id" element={<Navigate replace to="/takmicenja" />} /> }
                                {window.localStorage.getItem("role") === "ROLE_ADMIN" ?  <Route path="/takmicenja/kreiraj" element={<KreirajTakmicenje />} /> 
                                : <Route path="/takmicenja/kreiraj" element={<Navigate replace to="/takmicenja" />} /> }
                                <Route path="/login" element={<Navigate replace to="/takmicenja" />} />
                                <Route path="*" element={<NotFound />} />
                            </Routes>
                        </Container>
                    </Router>
                </div>
            );
        }else{
            return (
                <div>
                    <Router>
                        <Navbar expand bg="dark" variant="dark">
                            <Navbar.Brand as={Link} to="/">
                                JWD
                            </Navbar.Brand>
                            <Nav>
                                <Nav.Link as={Link} to="/login">
                                    Login
                                </Nav.Link>
                            </Nav>
                        </Navbar>
                        <Container style={{ paddingTop: "10px" }}>
                            <Routes>
                                <Route path="/" element={<Home />} />
                                <Route path="/login" element={<Login />} />
                                <Route path="*" element={<Navigate replace to="/login" />} />
                            </Routes>
                        </Container>
                    </Router>
                </div>
            );
        }
        
    }
};


ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
