import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import TestAxios from '../../apis/TestAxios';
import { withNavigation } from "../../routeconf";

class KreirajTakmicenje extends React.Component {

    constructor(props){
        super(props);

        let takmicenje = {
            mesto: "",
            naziv: "",
            formatId: "" ,
            datumPocetka: "", 
            datumZavrsetka: "",
        }

        this.state = {
           
          takmicenje: takmicenje,
            formati: []
        }

    }

    componentDidMount(){
        this.getFormati();
    }

    // TODO: Dobaviti filmove
    async getFormati(){
        try{
            let result = await TestAxios.get("/formati");
            let formati = result.data;
            this.setState({formati: formati});
            console.log("uspesno su dobavljeni formati");
            //alert("uspesno su dobavljeni formati");
        }catch(error){
            console.log(error);
            alert("formati nisu uspesno dobavljeni");
        }
    }

    async kreiraj(e){ 
        e.preventDefault();

        try{
            let takmicenje = this.state.takmicenje;
            console.log(takmicenje)

            let takmicenjeDto = {
               
                mesto: takmicenje.mesto,
                naziv: takmicenje.naziv,
                formatId: takmicenje.formatId,
                datumPocetka: takmicenje.datumPocetka,
                datumZavrsetka: takmicenje.datumZavrsetka
            }
            await TestAxios.post("/takmicenja", takmicenjeDto);
            //let response = await TestAxios.post("/vina", vinoDto);
            this.props.navigate("/takmicenja");
        }catch(error){
            alert("takmicenje nije kreirano");
        }
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let takmicenje = this.state.takmicenje;
        takmicenje[name] = value;
    
        this.setState({ takmicenje: takmicenje });
      }
    

    render() {
        return (
          <>
            <Row>
              <Col></Col>
              <Col xs="12" sm="10" md="8">
                <h1>Dodaj novo takmicenje</h1>
                <Form>
                  <Form.Label>Naziv takmicenja</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Naziv takmicenja"
                    name="naziv"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                  <Form.Label>Mesto odrzavanja</Form.Label>
                  <Form.Control
                    type="text"
                    name="mesto"
                    placeholder="Mesto odrzavanja"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                    <Form.Label >Datum pocetka takmicenja</Form.Label>
                    <Form.Control
                    type="date"
                    name="datumPocetka"
                    onChange={(e) => this.valueInputChanged(e)}
                     />
                    <Form.Label>Datum pocetka takmicenja</Form.Label>
                    <Form.Control
                        type="date"
                        name="datumZavrsetka"
                        onChange={(e) => this.valueInputChanged(e)}
                     />

                    <Form.Label>Format takmicenja</Form.Label> 
                    <Form.Control as="select" name="formatId" onChange={event => this.valueInputChanged(event)}>
                        <option>Izaberi format</option>
                        {
                            this.state.formati.map((format) => {
                                return (
                                    <option key={format.id} value={format.id}>{format.tip}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
    
                  <Button style={{ marginTop: "25px" }} onClick={(event)=>{this.kreiraj(event);}}>
                    Kreiraj takmicenje 
                  </Button>
                </Form>
              </Col>
              <Col></Col>
            </Row>                           
            
          </>
        );
      }
}

export default withNavigation(KreirajTakmicenje);