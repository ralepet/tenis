import React from 'react';
import TestAxios from '../../apis/TestAxios';
import { Row, Col, Button, Table, Form } from 'react-bootstrap'
import './../../index.css';
import { withParams, withNavigation } from '../../routeconf'

class Takmicenja extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            checked: false,
            takmicenja: [], 
            pretraga: { 
                mesto: "",
                formatId: ""
            },
            currentPage:0, 
            totalPages:0,
            formati: [],
            prijave: [],
            vecPrijavljen: false
        }
    }

    componentDidMount() {
        this.getTakmicenja(0);
        this.getFormate();
        //this.getPrijave();
        //this.proveriDaLiJePrijavljen();
    }

    /*
      async getPrijave(){
        try{
            let result = await TestAxios.get("/prijave");
            let prijave = result.data; 
            this.setState({prijave: prijave});
            console.log("prijave su uspesno dobavljene");
            //alert("prijave su uspesno dobavljene");
            //this.daLiJeVecPrijavljenNaTakmicenje(2); // ovde mu kao argument salje id
        }catch(error){
            console.log(error);
            alert("prijave nisu dobavljene");
        } 
    }
    */

    async getFormate(){
        try{
            let result = await TestAxios.get("/formati");
            let formati = result.data; 
            this.setState({formati: formati});
            console.log("formati su uspesno dobavljeni");
        }catch(error){
            console.log(error);
            alert("formati nisu dobavljeni");
        } 
    }

    async getTakmicenja(pageNo) {
        const config = {
            params: {
                pageNo: pageNo,
                mesto: this.state.pretraga.mesto,
                formatId: this.state.pretraga.formatId
            }
        }
        try {
            let result = await TestAxios.get('/takmicenja', config);
            let total_pages = result.headers["total-pages"]
            this.setState({
              takmicenja: result.data,
              currentPage: pageNo,
              totalPages: total_pages
            });
          } catch (error) {
            console.log(error);
          }
    }


    // OVA FUNKCIJA BI TREBALA DA PROVERI DA LI SE KORISNIK VEC PRIJAVIO NA NEKI TURNIR
    /*
    daLiJeVecPrijavljenNaTakmicenje(idKorisnika){
        let prijave = this.state.prijave;

        console.log(prijave)

        console.log(prijave[0].korisnikId)

        for(let i = 0; i < prijave.length; i++){
            if (prijave[i].korisnikId === idKorisnika){
                this.setState({ vecPrijavljen: true })
                return;
            }
        }
    }
    */
    

    idiNaKreiranje() {
        this.props.navigate('/takmicenja/kreiraj');
    }

    idiNaPrijavu(takmicenjeId) {
        this.props.navigate('/takmicenja/prijava/' + takmicenjeId);
    }

    obrisi(takmicenjeId) {
        TestAxios.delete('/takmicenja/' + takmicenjeId)
            .then(res => {
                // handle success
                console.log(res);
                alert('Takmicenje je uspesno obrisano!');
                window.location.reload();
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    idiNaPrethodnuStranicu(){
        this.getTakmicenja(this.state.currentPage-1) 
    }

    idiNaSledecuStranicu(){ 
        this.getTakmicenja(this.state.currentPage+1) 
    }


    // OVO SE MORA AKTIVIRATI NA SVAKU PROMENU
    onInputChange(event) {
        const name = event.target.name;
        const value = event.target.value

        let pretraga = this.state.pretraga;
        pretraga[name] = value;

        this.setState({ pretraga: pretraga })

        this.getTakmicenja(0);
    }

    
    renderTakmicenja() {
        return this.state.takmicenja.map((takmicenje, index) => {
            return (
                <tr key={takmicenje.id}>
                    <td>{takmicenje.naziv}</td>
                    <td>{takmicenje.mesto}</td>
                    <td>{takmicenje.datumPocetka}</td>
                    <td>{takmicenje.datumZavrsetka}</td>
                    <td>{takmicenje.formatTip}</td>
                    <td>{takmicenje.brojSlobodnihMesta}</td>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <td><Button variant="danger" onClick={() => this.obrisi(takmicenje.id)}>Obrisi</Button></td>
                        : <td></td>}
                    {window.localStorage.getItem("role") === "ROLE_KORISNIK" && takmicenje.brojSlobodnihMesta > 0 ?
                        <td><Button variant="primary" onClick={() => this.idiNaPrijavu(takmicenje.id)}>Prijavi se</Button></td>
                    : <td></td>}
                </tr>
            )
        })
    }


    render() {
        return (
            <Col>
                <Row><h1>Takmicenja</h1></Row>
                <>
                    <Form.Check onChange={() => this.setState({ checked: !this.state.checked })} label="Sakrij pretragu" />
                </>
                <Form hidden={this.state.checked} style={{ width: "100%" }}>                    
                   
                    <Form.Label>Format</Form.Label> 
                    <Form.Control as="select"  name="formatId" onChange={e => this.onInputChange(e)}>
                        <option value="">Izaberi format</option>
                        {
                            this.state.formati.map((format) => {
                                return (
                                    <option key={format.id} value={format.id}>{format.tip}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
                    <Form.Group>
                        <Form.Label>Mesto odrzavanja</Form.Label>
                        <Form.Control name="mesto" type="text" placeholder="Mesto odrzavanja" onChange={(e) => this.onInputChange(e)}></Form.Control>
                    </Form.Group>
                </Form>
                <br /><br />
                <Row>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <Button variant="success" onClick={() => this.idiNaKreiranje()}>Kreiraj takmicenje</Button> : null
                    }
                    <br /><br />
                <Button variant="info" disabled={this.state.currentPage===0} onClick={()=>this.idiNaPrethodnuStranicu()}>Prethodna</Button>
                <Button variant="info" disabled={this.state.currentPage===this.state.totalPages-1} onClick={()=>this.idiNaSledecuStranicu()}>Sledeca</Button>
                </Row>
                <Row>
                    <Table className="table table-striped" style={{ marginTop: 5 }} >
                        <thead className="thead-dark">
                            <tr>
                                <th>Naziv takmicenja</th>
                                <th>Mesto odrzavanja</th> 
                                <th>Datum pocetka takmicenja</th>
                                <th>Datum zavrsetka takmicenja</th> 
                                <th>Format</th>
                                <th>Broj slobodnih mesta</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderTakmicenja()}
                        </tbody>
                    </Table>
                </Row>
            </Col>
        );
    }
}

export default withNavigation(withParams(Takmicenja));