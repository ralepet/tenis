import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import TestAxios from './../../apis/TestAxios';
import {withParams, withNavigation} from '../../routeconf';

class Prijava extends React.Component {

    constructor(props){
        super(props);

        let prijava = {
            email: "",
            drzava: "",
        }

        /*
        this.state = {
          prijava: prijava, idTakmicenja: "", idKorisnika: ""
      }
      */
        this.state = {
            prijava: prijava, idTakmicenja: ""
        }

    }

    componentDidMount() {
        this.getTakmicenjeById(this.props.params.id);
     }
 
     getTakmicenjeById(takmicenjeId) {
        TestAxios.get('/takmicenja/' + takmicenjeId)
         .then(res => {
             // handle success
             console.log(res);
             this.setState({idTakmicenja: res.data.id});
         })
         .catch(error => {
             // handle error
             console.log(error);
             alert('Error occured please try again!');
          });
     }

    async prijaviTakmicara(e){ 
        e.preventDefault();

        try{
            let prijava = this.state.prijava;
            console.log(prijava)

            let prijavaDto = {

            takmicenjeId: this.state.idTakmicenja,
            kontakt: this.state.prijava.email,
            drzava: this.state.prijava.drzava,
            /*
            korisnikId: 2 // OVDE SE MORA PROMENITI ID KORISNIKA, 
            */
           
            }
            await TestAxios.post("/prijave", prijavaDto);
            this.props.navigate("/takmicenja");
        }catch(error){
            alert("Prijava nije uspesna");
        }
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let prijava = this.state.prijava;
        prijava[name] = value;
    
        this.setState({ prijava: prijava });
    }

    render() {
        return (
          <>
            <Row>
              <Col></Col>
              <Col xs="12" sm="10" md="8">
                <h1>Prijavi takmicara {this.state.idKorisnika}</h1>
                <Form>
                  <Form.Label>E-mail adresa</Form.Label> 
                  <Form.Control
                    type="text"
                    placeholder="Unesite e-mail adresu"
                    name="email"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                  <Form.Label>Drzava</Form.Label> 
                  <Form.Control
                    type="text"
                    placeholder="Unesite troslovnu oznaku drzave"
                    name="drzava"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                  <Button style={{ marginTop: "25px" }} onClick={(event)=>{this.prijaviTakmicara(event);}}>
                    Prijavi Takmicara
                  </Button>
                </Form>
              </Col>
              <Col></Col>
            </Row>                           
            
          </>
        );
      }


}

export default withNavigation(withParams(Prijava));